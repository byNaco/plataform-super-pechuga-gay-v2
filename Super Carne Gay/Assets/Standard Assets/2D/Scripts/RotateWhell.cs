﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class RotateWhell : MonoBehaviour {


    private Rigidbody2D rb;
    public float speed = 5000.0f;

	
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	
	void FixedUpdate () {
        rb.MoveRotation(rb.rotation + speed * Time.fixedDeltaTime);
    }
}
